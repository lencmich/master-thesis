\label[rtems]
\chap Real Time Executive for Multiprocessor Systems

This chapter introduces Real Time Executive for Multiprocessor Systems, the
main system used in this thesis, to the reader and discusses its usage and
advantages. Compilation steps for the target used for result demonstration
are presented at the end of the chapter.

The goal of this chapter is not to supplement comprehensive documentation
provided by RTEMS project itself, but to introduce its concepts used
for CAN/CAN FD stack implementation to readers not familiar with the
executive. RTEMS documentation should be studied to obtain information
about its concepts surpassing the scope of this thesis.

\label[rtems-introduction]
\sec Introduction

The development of Real Time Executive for Multiprocessor Systems, commonly
abbreviated as RTEMS, began in the late 1980s at the request of
US Army Missile Command for executive based on open standards and free
of royalties. Thanks to its open source license, RTEMS quickly became a
popular solution for embedded devices even in the commercial sector \cite[rtems-history].

The executive has come a long way since that and is a mature open
source real time operating system used in many critical fields such
as military, space, medical, or industry control systems. RTEMS provides
support for commonly used instruction set architectures including
ARM designs, x86 architecture, PowerPC, RISC-V or 32-bit i386 by Intel \cite[gedare-rtems].

It is compatible with POSIX standard but also provides its own interface used
mostly for kernel functions. Networking, if configured and enabled, is
supported by full-featured IPv4/IPv6 TCP/IP networking stack from FreeBSD
project as well as the DNS server. Most architectures have a subset of board
support packages (BSP) that supports symmetric multiprocessing (SMP) used
for targets with multiple processor cores.

Schedulers optimized for multiple core scheduling are used if SMP support
is enabled. Preemtive round-robin scheduling and timeslices for aperiodic tasks
are used. RTEMS has implemented support for several schedulers, both
fixed priority and dynamic task, fixed job priority for periodic tasks \cite[gedare-rtems].  

RTEMS has a shared memory architecture meaning all processes and threads may access the
same data stored in memory. It is a single address space system, therefore
application and kernel reside in the same memory \cite[gedare-rtems].

The kernel is written in C language, but the system also provides
support for the applications written in other programming languages as
C++, Ada, Java, or Lua for example. Modified
build automation tool "Waf" is used for the kernel and BSP build and provides the
benefit of having more builds of different architectures/BSPs available
all at once.


\label[rtems-resources]
\sec Resources

The following sections briefly describe RTEMS handlers and synchronization
routines used in the implementation of CAN/CAN FD stack and CAN controllers.
RTEMS implements most of the presented resources in both POSIX and its own API.
While POSIX API is a better choice for applications, RTEMS API (also called
classic) is the preferable choice for the kernel itself.

\label[rtems-task]
\secc Task

RTEMS documentation defines a task as the smallest thread of execution that
can compete on its own for system resources \cite[rtems-task-wiki]. Each task
has assigned a priority number from 1 to 255 with 1 being the highest priority.
The task is created by the directive "rtems_task_create()" and started by
"rtems_task_start()".

There are various settings available during task creation, for example
stack size available to the task, preemtion settings, interrupt level, and so on.
The infrastructure presented in this thesis uses tasks for the implementation
of controller workers responsible for frame transmission and reception.

The following code shows task creation and start with default options and
priority determined by "priority" variable. The task is implemented in
function "worker" with input argument "arg".

\begtt
rtems_task_create(
  rtems_build_name( 'N', 'A', 'M', 'E' ),
  priority,
  RTEMS_MINIMUM_STACK_SIZE + 0x1000,
  RTEMS_DEFAULT_MODES,
  RTEMS_DEFAULT_ATTRIBUTES,
  &task_id
);

rtems_task_start( task_id, worker, arg );
\endtt

\label[rtems-int]
\secc Interrupt Handler

Interrupts, requests for the processor to interrupt current execution and
process a different event, are managed through the interrupt manager in RTEMS.
This manager provides necessary directives to attach and manage interrupts.

Interrupt handler is installed with directive "rtems_interrupt_handler_install()".
This directive takes an interrupt vector number and installs
a routine called with an optional input argument when the interrupt occurs. The
following code shows a unique interrupt handler installation with interrupt
number "irq" calling routine "interrupt_handler" without an argument.

\begtt
rtems_interrupt_handler_install(
  irq,
  "name",
  RTEMS_INTERRUPT_UNIQUE,
  interrupt_handler,
  NULL
);
\endtt

The manager also provides synchronization via
"rtems_interrupt_lock_acquire()" and "rtems_interrupt_lock_release()" directives.
These functions are used to protect shared data between an interrupt handler
and the thread owning the handler. Maskable interrupts are disabled
during interrupt lock.

These locks were initially used for CAN FIFO queues synchronization before
switched for the mutex mechanism described in section \ref[rtems-mutex].

\label[rtems-semaphore]
\secc Semaphore

Self-contained binary semaphores (mechanism with integer value 0 or 1) are
used to implement critical sections in the infrastructure (such as
read/write operations) or to wait for some event (until all frames are sent
for example).

Both static (compile time) and dynamic (run-time) initialization of the
semaphore is available. Macro "RTEMS_BINARY_SEMAPHORE_INITIALIZER" is used
for the first one and function "rtems_binary_semaphore_init()" for the latter.
Wait for the semaphore is implemented in "rtems_binary_semaphore_wait()" function.
It returns immediately if the current semaphore value is 1, otherwise the thread
is blocked and waits for semaphore to change its value to 1. Since this can
wait indefinitely, the user can use "rtems_binary_semaphore_wait_timed_ticks()" to
wait with a defined timeout or "rtems_binary_semaphore_try_wait()" to return
with an error if the semaphore is not set.

Function "rtems_binary_semaphore_post()" wakes up the highest priority waiting
thread or increments the value to 1 if no thread currently waits on the
semaphore.

Examples of semaphore usage can be found in section \ref[rtems-ctucanfd-implementation]
describing the implementation of CTU CAN FD controller.

\label[rtems-mutex]
\secc Mutex

Mutual exclusion, often referred to as a mutex, is a synchronization mechanism
ensuring a task does not enter a critical section if another task is already
present in it. RTEMS implements mutual exclusion with self-contained
objects. These can be initialized either statically with macro
"RTEMS_MUTEX_INITIALIZER" or dynamically at run-time with "rtems_mutex_init()"
function.

Locking is performed with "rtems_mutex_lock()" call, unlocking can be done with
"rtems_mutex_unlock()". The CAN/CAN FD stack was initially implemented with
more lightway and general interrupt locks for synchronization but later was rewritten
to mutual exclusion as it is more fitting for this usage. It is a more 
systematic solution that ensures CAN worker or CAN API calls cannot delay
higher priority tasks when CAN related tasks are iterating inside CAN
subsystem.

However, there are some disadvantages in this approach. Usage of mutexes does
not allow to fill the CAN stack FIFO queues directly from an interrupt, but controller
has to use a dedicated thread for this. Also, the choice of mutexes over
interrupt locks caused small slowdown of CAN stack about approximately
6 micro seconds per frame.

\label[rtems-build]
\sec Build Process

RTEMS build can be separated into three major steps: setting up the build system,
BSP build and build of the networking stack (can be omitted if
network subsystem is not required). The build example is done for ARM based Xilinx Zynq
ZedBoard BSP used for result demonstration in this thesis.

\secc Build System Setup

Source directories can be obtained either from project GIT repositories or
stable releases can be downloaded from project websites. These steps present
the first option as the work of the thesis is done against current development version
of RTEMS.

\begtt
mkdir rtems-git
cd rtems-git
git clone https://gitlab.rtems.org/rtems/rtos/rtems.git
git clone \
https://gitlab.rtems.org/rtems/tools/rtems-source-builder.git rsb
git clone https://gitlab.rtems.org/rtems/pkg/rtems-libbsd.git

\endtt

Now with source directories obtained, the target directory for system build
has to be selected. Directory "/opt/rtems/6" is used in this example. This is
the path where RTEMS build target specific dependencies (GCC, GDB or Binutils
for example) are located. These utilities can be built by following command.

\begtt
cd rsb/rtems
../source-builder/sb-set-builder --prefix=/opt/rtems/6 6/rtems-arm
\endtt

Other architecture ("rtems-i386" for example) can be specified instead of
"rtems-arm" and more architectures can be built with common "/opt/rtems/6"
prefix as source
builder creates specific subdirectory for each architecture.

\secc BSP Build

With architecture dependencies built and ready, board support package
can be built. The initial configuration for Xilinx Zynq ZedBoard is set up
by following command.

\begtt
cd rtems-git/rtems
./waf bspdefaults --rtems-bsps=arm/xilinx_zynq_zedboard > config.ini
\endtt

File "config.ini" can be edited and configuration changed based on user
requirements (for example "RTEMS_POSIX_API" and "RTEMS_SMP" might be enabled).
Following options were modified in our build for our BSP support.

\begtt
RTEMS_POSIX_API = True
RTEMS_SMP = True
ZYNQ_UART_KERNEL_IO_BASE_ADDR = ZYNQ_UART_0_BASE_ADDR
\endtt

Once configured, board support package can be installed to
previously selected "/opt/rtems/6" with following commands.

\begtt
./waf configure --prefix "/opt/rtems/6"
./waf
./waf install
\endtt

\secc FreeBSD Library Build

FreeBSD Library is required if networking stack (support for TCP/IP protocol
for example) is used. It is once again built using Waf tool.

\begtt
cd rtems-git/rtems-libbsd
git submodule init
git submodule update rtems_waf
./waf configure --prefix="/opt/rtems/6" \
  --rtems-bsps=arm/xilinx_zynq_zedboard \
  --buildset=buildset/default.ini
./waf
./waf install
\endtt

\label[rtems-omk]
\sec Application Build with OMK

For the development and testing, OMK Make-System\urlnote{http://rtime.felk.cvut.cz/omk/}
was used to build the CAN/CAN FD stack, CTU CAN FD driver, and test applications.
This section shows how to build the source code to reproduce the demonstration
and results documented in chapter \ref[results]. It is currently possible to
reproduce most of the tests on both Xilinx Zynq target and i386 architecture
using mainline x86-64 QEMU. The source code can be obtained from CTU FEE GitLab
repository.

\begtt
git clone https://gitlab.fel.cvut.cz/otrees/rtems/rtems-canfd.git
\endtt

\secc Xilinx Zynq MzAPO Board

The build for Xilinx Zynq board is straightforward once the BSP is compiled
and installed as described in previous sections.

\begtt
export PATH=$PATH:/opt/rtems/6/bin
make
\endtt

These commands builds the RTEMS kernel image with CAN/CAN FD stack, CTU CAN FD
driver, and test applications. The image can be loaded to the board with
boot from Trivial File Transfer Protocol (TFTP), the detailed description
is available on OTREES wiki pages.\urlnote{https://gitlab.fel.cvut.cz/otrees/rtems/work-and-ideas/-/wikis/home}

\secc i386 using QEMU

The presented CAN/CAN FD stack can also be tested using mainline QEMU
"qemu-system-x86_64" by compiling the application for i386 target. It is
necessary to build and install support for "i386_pc686" RTEMS targer. The
steps are similar for Xilinx Zynq target described above. Once the build
system is setup, the following may be used to compile and install BSP support.

\begtt
cd targets/i386_pc686
./i386-rtems-sys.cfg
\endtt

This script automates board support package installation. It is possible to
run QEMU target with following commands.

\begtt
export PATH=$PATH:/opt/rtems/6/bin
cd targets/i386_pc686
./setup-host-socketcan # might be required with sudo
./qemu-i386-pc686-2x-ctu-pci-build
./qemu-i386-pc686-2x-ctu-pci-run
\endtt
