\label[introduction]
\chap Introduction

Real Time Executive for Multiprocessor Systems\urlnote{https://www.rtems.org/}, abbreviated as RTEMS, is an
open source real time operating system (RTOS) designed mainly for embedded
devices. It supports POSIX application programming interface, making
the designed application compatible with for example GNU/Linux or NuttX
operating systems as well. RTEMS is widely used in critical real time control fields, such
as space systems, medical devices, or networking systems. These are the fields
where transfers of critical messages between used devices are usually required and
Controller Area Network (CAN) bus standard is an ideal choice to ensure
the highest priority message gets precedence over lower priority ones in
case of collision on the bus.

However, the executive does not have a general purpose CAN/CAN FD stack
implementation that would provide a common application interface. Instead of
that, target dependent solutions in applications/BSPs are required for
current CAN drivers. This might be sufficient for some applications but a common
stack with blocking and nonblocking access, priority classes, or message filtering
is a necessity for more complex usage.

The main goal of the thesis was to design and implement a common CAN/CAN FD stack
that would implement features needed for full utilization of CAN bus, provide
the missing unification of the application interface for CAN
controllers and simplify both the porting of new drivers and CAN bus usage
from the application perspective. The presented implementation of RTEMS common CAN stack is
based on the Linux kernel loadable module LinCAN developed at Czech Technical
University in Prague by Pavel Píša in the early 2000s and subsequently used in
real time applications for decades. It is based on message FIFO queues,
organized into oriented edges between chip drivers and CAN users, that
are responsible for message transfers from the application to the controller and
vice versa.

A common setback of general purpose CAN drivers utilizing software FIFO queues
is the bus arbitration priority inversion problem. It occurs when
the bus is saturated by middle priority messages from one controller and a mix
of low and high priority messages to transmit is pending on the other
controller. In that case, high priority message is blocked by medium
priority one and thus priority inversion scenario occurs.

The usual solution is to introduce priority classes assigned from CAN message
identifier ranges and route messages to different queues based on those classes.
Mapping priority classes to a limited count of the controller's hardware transmission
buffers, however, tends to be challenging. This is caused by controllers usually providing
transmission of messages based on their CAN identifiers or in the fixed order determined
by the TX buffer index.

However, the transmission order based on CAN identifiers can not be used if the
preservation of the message order determined by the application is required. On the
other hand, using more priority FIFO classes requires sending messages in the order
determined by those classes. As a result, the correct transmission order should
send messages based on their priority class and keep the order within one
priority class. This usually forces the CAN driver to limit transmission
to one buffer per class or even to one buffer at all and thus not utilize the full
potential of the controller.

The solution proposed in scope of this thesis solves arbitration priority inversion
problem by extending the common solution of FIFO queues for each priority class.
This solution of multiple traffic classes is extended by adding the dynamic
redistribution of CAN transmission buffers to these classes. This way, the controller may
use all its hardware transmission buffers, and the correct transmission order is preserved.
Open source CTU CAN FD IP core developed at Czech Technical University was chosen for
the demonstration of presented solution. It supports the abort of currently
queued TX buffer and the buffer priority updates, which changes the requested
buffer transmission order, on the fly.

The thesis starts with chapter \ref[rtems] introducing the reader to RTEMS
executive, its basic principles, and compilation steps for thesis results
reproduction. The core part of the thesis, the implementation of new full-featured
CAN/CAN FD stack to RTEMS is described in chapter \ref[rtems-can]. The
implemented infrastructure is described from both theoretical and implementation
based perspective. The next chapter faces the priority inversion problem on CAN bus
and proposes a solution that can be used with the implemented infrastructure.
This chapter is more focused on theoretical part. Practical impacts are discussed in chapter
\ref[ctucanfd] together with the implementation of CTU CAN FD controller.
The thesis is completed with chapter \ref[results] presenting the achieved
results.

The text is partially based on a science article published at the
18th international CAN Conference in 2024 \cite[icc-article].