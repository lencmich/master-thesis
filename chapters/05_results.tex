\label[results]
\chap Results and Demonstrations

This chapter provides results and demonstrations of RTEMS CAN/CAN FD stack
developed in scope of this thesis and thoroughly described in chapter
\ref[rtems-can]. Various latency profiles were measured to evaluate the
stack performance. This chapter also briefly describes used test
applications and demonstration with pysimCoder rapid development tool.

All tests and demonstrations were done on educational kit MicroZed APO. This
kit is used in many computer science coursers at CTU FEE and is based on
MicroZed evaluation kit with Xilinx Zynq-7000 system on chip.

\label[results-stack]
\sec RTEMS CAN Stack Mutual Latency Profiles

RTEMS CAN stack latencies were partially tested on LaTester infrastructure
originally developed for continuous CAN bus latency testing on Linux kernel \cite[jerabek-thesis].
In this setup, the device under test (DUT) functions as a CAN gateway:
receives messages from CAN bus A and instantly sends it to CAN bus B.
The measuring system generates and receives CAN/CAN FD frames and provides
precise timestamping measurement. The connection of this framework is
available in Figure \ref[latester].

\medskip \clabel[latester]{CAN LaTester Visualization}
\picw=10cm \cinspic figs/latester.pdf
\caption/f CAN LaTester connection scheme (Source: \cite[vasilevski-latency]).
\medskip

The latency measuring starts once CAN frame is sent to the bus and ends once
the entire message is received. This is depicted in Figure \ref[latency-diagram].
The length of the message on the link at given nominal and data bit rates is subtracted
from the latency. The final result is the time it takes the DUT to read received
message from CAN bus A controller, process it and forward it into TX
buffer of the controller connected to CAN bus B. In other words,
it is a time the frame spent in software stack \cite[vasilevski-latency]
\cite[pisa-embedded]. The advantage of using the same hardware and framework
as for Linux kernel tests is possibility to compare RTEMS and GNU/Linux
CAN latencies, respectively character device stack and SocketCAN latencies,
in the future.

\medskip \clabel[latency-diagram]{CAN Message Latency Measurement}
\picw=10cm \cinspic figs/latency_diagram.pdf
\caption/f CAN message latency measurement (Source: \cite[vasilevski-latency]).
\medskip

The testing framework has several options for which the latencies can be
measured. Both standard and FD frames can be tested and modes "once" and
"flood" are available. The latter floods the bus, while mode "once" always
waits for the message to be received back before sending another one.
Measured latencies for system without any other tasks (such as network)
can be seen in Figure \ref[stack-latency].

\medskip \clabel[stack-latency]{RTEMS CAN Stack Latency Profile}
\picw=17cm \cinspic figs/stack_latency.pdf
\caption/f RTEMS CAN stack read to write latency profile.
\medskip

Another latency measurement was done with enabled networking stack and 
MicroZed APO board hosting a simple HTTP web server displaying dummy data. This
was done to provide some way to stress the networking stack. Priorities of
CAN tasks were selected higher compared to networking and web server tasks.

Two types of measurements were done for enabled networking. At first,
latencies were measured with enabled networking stack and running web server.
Then, the server was stress tested with "siege" tool providing additional load
for the networking stack. Since CAN priorities were selected the highest,
the expectation for the measurement without web server stress are the latencies
approximately the same as without networking stack at all. These values
should get worse if hosted HTTP web server is stress loaded by an external tool.

\medskip \clabel[stack-latency-net]{RTEMS CAN Stack Latency Profile with Networking}
\picw=17cm \cinspic figs/stack_net_latency.pdf
\caption/f RTEMS CAN stack read to write latency profile with enabled networking.
\medskip

The measurement results can be seen in Figure \ref[stack-latency-net]. The
bold full lines are the data measured with stress loaded HTTP server, dashed
thin ones are without external load. It can be seen latencies without load are just
slightly worse than the ones with disabled networking presented in Figure \ref[stack-latency].
The results got worse when HTTP server is stress loaded, but still those
values are not bad. Maximum latency difference is up to 20 micro seconds,
which can be caused by networking critical sections with disabled interrupts,
interrupt handling, context switching between cores, competing for cache
memory between CAN and network tasks and so on. Overall, the
measured latencies, both with and without networking, look really promising.

It should be noted that RTEMS executive has single address space. Therefore,
CAN gateway used in these measurements is basically kernel gateway and not
user space gateway.

\label[results-dynamic]
\sec Dynamic Allocation Demonstration with RTEMS and CTU CAN FD

The ability of dynamic allocation of TX buffers to priority groups,
presented and described in chapter \ref[can-prio] was
demonstrated on educational kit MicroZed APO. The programmable logic part was
configured with four independent CTU CAN FD IP cores/CAN FD controllers, each
one with four TX buffers. The application ran on RTEMS executive and used the
newly introduced common CAN/CAN FD stack.

The goal of the test was to demonstrate the ability of high priority messages
to preempt low priority messages located in controller's HW buffers. To achieve
this, one controller was fully loading the CAN bus with 8 bytes long messages.
These messages had identifier 0x500 that served as middle priority identifier.
The second controller was accessed from two separate applications with
one attempting to send messages with 0x700 identifier (low priority message)
without delay between messages (basically flood) and second attempting to
send 8 byte long messages with 0x20 identifier.

\medskip \clabel[arbitration-fail]{CAN Bus State without Dynamic Allocation}
\picw=14cm \cinspic figs/arbitration.pdf
\caption/f CAN bus state without dynamic allocation of TX HW buffers. Priority inversion occurs.
\medskip

If only traditional driver with FIFOs would be used, the high priority
messages would wait indefinitely for low priority ones to leave four
TX buffers. Those would of course never leave, because the bus was fully
loaded by middle priority ones and thus low priority message would never
win the arbitration phase. This situation is depicted in Figure \ref[arbitration-fail],
where controller A propagates low priority to arbitration phase. These frames
of course lose the arbitration to middle priority frames sent from controller B.

\medskip \clabel[latency-profile]{High-priority message latency profile}
\picw=17cm \cinspic figs/latency_profile.pdf
\caption/f High priority message latency profile.
\medskip

The high priority messages were sent in the burst of size 4 and the
application subsequently waited long enough to send those messages to
the bus. These messages were passed to the controller through the queue with
higher priority and filter set to match only 0x20 identifier. The latency
profile in Figure \ref[latency-profile] shows the write-to-receive latency
in microseconds for 10,000 sent high-priority messages. RX side timestamps
are captured at Start of Frame bit and delivered in frame's timestamp
field to the test application. The TX time is read from the CTU CAN FD IP core
by "RTEMS_CAN_CHIP_GET_TIMESTAMP" ioctl call (refer to section \ref[can-hwtimestamp])
exactly before frame write operation.

The black vertical lines represent the length of one 8-byte long message
transmission (about 130 microseconds). It can be seen the first message
is transmitted to the network almost immediately in the best case. The delay is caused by
framework latency (time it takes to propagate the frame through queues to
the controller and save it to HW buffer, see section \ref[results-stack]).
The worst case is given by an interval corresponding to the blocking of
transmission by an already started middle priority message transmission that
occupies the bus. Subsequent three messages follow immediately after
the previous ones.

From bus point of view, high priority frames are now propagated to the
arbitration phase correctly and win the arbitration against frame from
controller B. This can be seen in Figure \ref[arbitration-fix].

\medskip \clabel[arbitration-fix]{CAN Bus State with Dynamic Allocation}
\picw=14cm \cinspic figs/arbitration_fix.pdf
\caption/f CAN bus state with dynamic allocation of TX HW buffers.
\medskip

\sec Stack Functionality Demonstration with pysimCoder

The framework was also tested against pysimCoder rapid application development
tool. It is developed by Professor Roberto Bucher as an open source project
with the goal to provide alternative to proprietary Matlab/Simulink for
educational purposes and possibly even for simple industrial
applications \cite[pysim-article].

There is an active development effort towards pysimCoder at CTU, including
support for NuttX operating system or run time monitoring and tuning of
model parameters. In this demonstration, pysimCoder was ported to RTEMS
and simple application demonstrating CAN frames send and receive was created.

The initial version of common CAN bus interface was also introduced for
this demonstration. Application generated by pysimCoder consists of multiple
blocks. Target specific blocks, motor control block for example, may use
CAN bus to communicate with the hardware and there may be more than one
of these blocks in the diagram. In this case, each block usually listens
to different CAN frames (different identifier). Some common pysimCoder
interface, capable of registering messages to be received, is useful in this
case.

In the developed interface, pysimCoder's blocks registers identifier
to receive during their initialization. This creates one slot for the message
with given identifier in pysimCoder's common interface. Block can then
read from this slot during its periodically called in/out function. CAN
frame read/write is implemented in target specific code with frame reception
done in a separate thread. Frame transmission is performed as blocking write.

\medskip \clabel[pysim-can-interface]{pysimCoder CAN interface}
\picw=15cm \cinspic figs/pysim-can-interface.pdf
\caption/f Visualization of pysimCoder common CAN interface.
\medskip

Visualization of the interface can be seen in Figure \ref[pysim-can-interface].
In this example, both blocks A and B want to read the frame with identifier
A. Therefore, the interface creates two slots and saves the message in both
of them when received. Frame transmission from blocks to network also passes
through the interface for code unification, but it is passed directly to the
target dependent code. Since CAN frame structure differs from system to system,
pysimCoder just passes the desired identifier, data and optionally flags.
Target dependent code then assigns these values to its CAN frame.

The common CAN interface for pysimCoder's blocks is still under development
and not yet ready to merged into mainline. At the moment, only RTEMS is
partially ported to this interface with other systems (Linux and NuttX) still
using the old interface.

\sec OrtCAN CAN/CANopen

An experiment to include new RTEMS CAN/CAN FD API option into OCERA OrtCAN
CAN/CANopen infrastructure has been implemented by Pavel Píša in
"ortcan-vca/libvca/vca_irtems.c"\urlnote{https://sourceforge.net/p/ortcan/ortcan-vca/ci/master/tree/libvca/vca_irtems.c}.
The code can be build with actual RTEMS even together with PMSM control
application using RVapo RISC-V coprocessor\urlnote{https://gitlab.fel.cvut.cz/otrees/fpga/rvapo-vhdl} and
testing is planned in the following months.

\sec Documentation

Documentation is automatically generated by a tool named Doxygen. It extracts
information from comments in source files and headers and generates description
of structures, functions, defines, files and so on. The descriptions with links
are very useful when going through used functions and arguments for example. The
link to the generated documentation can be found in project repository\urlnote{https://otrees.pages.fel.cvut.cz/rtems/rtems-canfd/doc/doxygen/html/index.html}.

Another piece of created documentation is a user manual. It refers to how to
use the infrastructure from user point of view and also how to port a new driver
to it\urlnote{https://otrees.pages.fel.cvut.cz/rtems/rtems-canfd/doc/can/can-html/can.html}.

\sec Test Applications

Several test applications were written during the stack implementation to
test the principle and measure performance. These are all linked
during build steps described in sections \ref[rtems-build] and \ref[rtems-omk].
This section introduces them and informs the readed how to use them
to reproduce thesis's results.

\secc can_register

This application registers CAN device/controller into "dev/canX" namespace.
Simple iteration from zero is used, therefore first registered device will
get "dev/can0", second "dev/can1" and so on. Type of device to register
is selected with "-t" parameter; it can be either "ctucanfd" or "virtual"
target.

\nl
{\bf Example:}
Initialization of CTU CAN FD driver would be called as
\begtt
can_register -t ctucanfd
\endtt
This would register two CTU CAN FD controllers under "dev/can0" and
"dev/can1" (suppose these are the first two registered controllers).
\nl

This is a process that would usually be handled from board support layer
in RTEMS based on values obtained from device tree. However, since the
stack is not merged to upstream at the time of writing this thesis, all
tests were done from application layer against clean RTEMS build. Therefore,
this application substitutes board level initialization for CAN device.

\secc can_list_registered

This application can be used to list all devices previously registered
with "can_register" application.

\secc can_set_test_dev

It is necessary to set which device drivers shall be used for testing.
Application "can_set_test_dev" is used for this purpose. It takes
to device drivers as input arguments and uses them for subsequent
test applications.

\nl
{\bf Example:}
Previously initialized CTU CAN FD controller can be set for tests as
\begtt
can_set_test_dev dev/can0 dev/can1
\endtt

The order of parameters matters. In case of 1 way test -- please refer
to section \ref[can-test-1way] -- first parameter is used as a sender
and second one as a receiver. For 2 way test -- refer to section \ref[can-test-2way]
-- both devices send and receive. It is necessary to specify both parameters
even if they are the same devices (as it is for virtual target for example).

\label[can-test-1way]
\secc can_1way

This application performs one way test where one device acts as a sender
and other one as a receiver. Used devices are specified by "can_set_test_dev"
application.

\begtt
can_1w [count] [delay] [burst]
\endtt

It sends "[count]" number of messages with burst size "[burst]" and
delay between bursts specified by "[delay]" arguments. 

\label[can-test-2way]
\secc can_2way

This test is the similar to the previous one, except frame transmission is
performed in both ways. Once again, parameters "[count]", "[delay]" and
"[burst]" can be specified. This applies to both devices in this case.

\begtt
can_2w [count] [delay] [burst]
\endtt

\secc can_gateway

This application implements CAN gateway -- an interface between two
CAN buses, possibly with different bit timing settings. It receives messages
on the first device defined with "can_set_test_dev" application and
sends it to the seconds one.

This application is used for mutual latency testing described in
section \ref[results-stack]. Since RTEMS is a single address space
operating system, the application also acts as a kernel gateway,
although implemented in application space.

\secc can_latency

This application performs latency testing with priority inversion
occuring on the bus. One device is flooding the bus with middle priority
frames while the other is trying to send mix of low and high priority
messages. Please refer to chapter \ref[can-prio] and section \ref[results-dynamic]
for further description.
