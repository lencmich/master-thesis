\label[can-prio]
\chap CAN Bus Priority Inversion Problem

This chapter introduces CAN bus arbitration priority inversion problem
to the reader and proposes a solution by introducing the TX buffers dynamic
allocation mechanism. This chapter deals with the theoretical part of the
problem and its solution. The implementation of presented algorithm to
CTU CAN FD target controller is described in the following chapter,
results can be found in section \ref[results-dynamic].

\label[can-prio-introduction]
\sec Introduction

CAN bus arbitration phase ensures the message with the highest priority (i.e.
the lowest identifier) preempts the messages with lower priority (i.e. higher
identifier). A problem occurs when the bus is fully saturated by middle priority
messages from one or more controllers and a mix of low and high priority
messages is pending on the other controller. If all controller's hardware TX
buffers are filled with these low priority messages, middle priority messages
will always take precedence over low priority ones. This means that high
priority messages will not be sent until other controllers stop or
pause their messages stream (or indefinitely).

The problem, when low priority message, correctly preempted by middle priority
message, is holding a resource (hardware TX buffers) required by high priority
message and thus indirectly delaying the execution of it, is called
priority inversion in computer science. The problem mostly occurs in
task scheduling, however it can affect CAN bus as well.

\medskip \clabel[can-prio-inv-viz]{CAN Priority Inversion Visualization}
\picw=15cm \cinspic figs/can_prio_inv.pdf
\caption/f Visualization of CAN Bus Arbitration Priority Inversion.
\medskip

The priority inversion problem is visualized in Figure \ref[can-prio-inv-viz].
In this visualization, controller B floods the bus with "0x500" identifier
while controller A tries to send a mix of "0x20" and "0x700" identifiers.
However, if all hardware buffers are already filled with low priority "0x700"
identifier, high priority "0x20" will never get to the arbitration phase
and is incorrectly held by the middle priority message on the bus as a result.

\sec Common Solutions

This problem is usually solved by introducing priority classes and messages
being routed to different queues based on their priority class assigned from
CAN message identifier ranges. Mapping priority classes to a limited count
of the controller's hardware transmission buffers however tends to be
challenging. This is caused by controllers usually providing transmission
of messages based on their CAN identifiers or in the fixed order determined
by the TX buffer index.

However, application and sometimes even protocol implementations usually require
the preservation of message order, even if different CAN identifiers are used.
This common requirement disqualifies the message transmission order
based on CAN identifier. On the other hand, more priority FIFO classes lead to
the need to send messages in the order determined by those classes. Usually,
the driver limits the transmission to one buffer per class or even to one
TX buffer at all and thus not using the full potential of the controller.

\label[can-prio-dynamic]
\sec Dynamic Allocation of TX Buffers to Multiple Priority Groups

The priority inversion problem described above can be solved by adding the
dynamic redistribution of CAN transmission buffers to priority classes. This
approach therefore keeps the common solution of assigning CAN messages
to priority classes based on identifier range and having a FIFO queue for each
class.

After the message is released by the application to the queue
structure, it is inserted into the proper priority class based on the
identifier match filter. The controller’s driver is notified of the new
TX message to be processed and checks whether it has a space in hardware
buffers.

If there is a free space available in hardware buffers, the message is
inserted into the free one. The insertion of the new message into one of the
buffers is followed by transmit sequence reorganization to ensure the correct
transmission order based on both priority class and application defined order.
In general, the buffer with newly inserted message has to be placed in the
transmit sequence after all messages of the same or higher priority class
but before all messages of a lower priority class. This way the controller
ensures the messages from the higher priority class are sent first and also
the order of messages within the same priority class, defined by one or more
applications, is preserved.

If no space is available, the controller checks whether a message of lower
priority class occupies the buffers. If yes, the last message of the lowest
priority class occupying TX buffer is replaced by the pending message and
scheduled for later processing. Transmit sequence reorganization follows if the
new message is inserted. The message replacement poses some requirements on
both CAN stack and controller. The stack has to support pushback messages and
must schedule them for later processing, so they are not lost, and the controller
must be able to abort TX buffer processing if needed. The algorithm requirements
on both the software stack and CAN controller are discussed in detail in section
\ref[can-prio-inv-req].

\label[can-prio-class-map]
\secc Priority Classes Mapping

The sequence of buffers to be transmitted is stored in the TX order array
holding the numbers of hardware buffers as they should be processed. The
priority classes are mapped as continuous ranges in this array using TX order
tail array where each entry represents the tail index in the transmission
sequence array for a given priority class.

The tail points one position beyond the last allocated slot for the corresponding
class. Only the tail is needed as the head for the highest priority class is
fixed and heads for lower priority classes are at the exactly same position as
the previous priority class tail. This tail is used for the insertion of new
frames if the array is not full or for reorganization when inserting new
frames into the array.

\secc Example

This example is provided for CAN stack with three priority classes
with priorities 0 (low), 1 (middle), and 2 (high) and a controller with 4
HW buffers. The application(s) passed four messages to the controller in this
order: low priority message, high priority message, low priority message and
high priority message.

The relationship between hardware buffers, SW queues, and the usage of TX
order array mapping is visualized in Figure \ref[classes-mapping]. The
messages are inserted into the following hardware buffers: first low priority
message to buffer 0, first high priority message to buffer 3, second low
priority message to buffer 2, and second high priority message to buffer 1.
The buffer order does not match the insertion order because other messages
might have been inserted into those buffers previously, and therefore they
were replaced in different order. Moreover, the buffer numbering is not
important here as the transmission sequence is defined TX order array.

\medskip \clabel[classes-mapping]{Dynamic Allocation of TX buffers}
\picw=9cm \cinspic figs/classes_mapping.pdf
\caption/f Visualization of dynamic allocation of TX buffers.
\medskip

Without rotation of buffer priorities, this insertion order would mean the
controller would try to send a low-priority message first and in a better case
delay a high-priority message or in a worse case cause priority inversion problem.
However, the presented approach reorganizes the hardware buffer priorities
and as a result, buffers 3 and 1 are sent in prior to buffers 0 and 2,
ensuring the correct priority and application-based order. TX order tail
array moved priority classes tails to position 2 for high and middle priority
classes, therefore new messages from those classes would be inserted there
and lower priority classes would move left. Note that the tail has to be
moved also for all lower priority classes, therefore middle priority class
is moved as well.

Adding a new high priority or a middle priority message
would result in low priority from TX buffer 2 transmission deactivation and
being pushed to software FIFO if not sent yet. Buffer 2 would then be
reused for the new message. The situation, where low priority message in
buffer 2 was replaced by middle priority message and transmission order
was correctly reorganized, is depicted in Figure \ref[classes-mapping2]

\medskip \clabel[classes-mapping2]{Dynamic Allocation of TX buffers with Middle Priority}
\picw=9cm \cinspic figs/classes_mapping.pdf
\caption/f Visualization of dynamic allocation of TX buffers after middle priority frame insertion.
\medskip

The results of presented mechanism and demonstration on real hardware are
available in section \ref[results-dynamic]. The mechanism is presented
on RTEMS CAN infrastructure and CTU CAN FD core on educational kit MicroZed
APO.

\label[can-prio-inv-req]
\secc Algorithm Requirements

While the algorithm is generic and not tied to the presented infrastructure
or CTU CAN FD controller, there are some requirements both stack and controller
have to fulfil to use this solution. This section presents them and provides
some possible workarounds if available.

An obvious requirement for CAN stack is the support of multiple priority queues.
The number of queues is up to stack implementations, but at least three possible
queues should be considered to satisfy application requirements. The stack
also has to provide a feature to return frames passed to the controller back
to the FIFO in case of buffer abort. The returned message should be scheduled
for later processing. In general, this means the stack has to keep the slot
allocated until notified of successful transmission or error. This, in fact,
is also done in SocketCAN, therefore possible extension to Linux kernel
should be possible.

From the controller side, the requirements might be a bit more demanding. There
must be a way to determine the order in which the transmission hardware
buffers should be sent to the network. This might be a problem for some
controllers that do not support local priority for message buffers but
only consider the identifier of the inserted frame or even just fixed FIFO order.
The possibility to change the priorities of hardware buffers
on the fly -- without deactivating them -- provides a huge advantage for the
presented algorithm. Without this feature, the controller would have to
disable all buffers for which the priority shall be changed, modify the
value, and enable them again. This might cause a small delay between
frame transmissions. Hardware buffer abort feature is also required, but
this is supported by most of the current controllers. 
